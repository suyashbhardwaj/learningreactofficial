/*Sections covered in this program*/
/*
Component Helper Functions, 
Destructuring, 
Stateful Component 
Event Handlers 
An event handler is a function
Passing state - to child components
Changes in state cause rerendering
Refactoring the components
*/
import React from 'react'
import { useState } from "react"

// const Display = (props) => {
const Display = ({ counter }) => {
    return (
    // <div>The counter is set to {props.counter}</div>
    <div>The counter is set to {counter}</div>
    )
    }
      
// const Button = (props) => {
const Button = ({ handleClick, text }) => {
    return (
    // <button onClick={props.handleClick}> {props.text} </button>
    <button onClick={handleClick}> {text} </button>
    )
    }

const Explainer1c = () => {
    const [ counter, setCounter ] = useState(0)
    console.log('rendering with counter value', counter)
  
    const handleClick = () => {
      console.log('clicked')
    }
  
    const increaseByOne = () => { console.log('increasing, value before', counter); setCounter(counter + 1)}
    const setToZero = () => { console.log('resetting to zero, value before', counter); setCounter(0)}
    const decreaseByOne = () => { console.log('decreasing, value before', counter); setCounter(counter - 1)}
    return (
    <div>
        {/* <div>{counter}</div> */}
        <Display counter={counter}/>

        {/* <button onClick={handleClick}> */}
        {/* <button onClick={() => console.log('clicked to no redirect')}>
        plus
        </button> */}
        {/* <button onClick={() => setCounter(counter + 1)}>
        plus
        </button>
        <button onClick={() => setCounter(0)}>         zero      </button> */}
        {/* <button onClick={increaseByOne}>plus</button>
        <button onClick={setToZero}>zero</button> */}
        <Button handleClick={increaseByOne} text='plus'/>
        <Button handleClick={setToZero} text='zero'/>
        <Button handleClick={decreaseByOne} text='minus'/>
    </div>
  )
}

export default Explainer1c