/*Sections covered in this program*/
// Update of the state is Asynchronous
import React from 'react'
import { useState } from "react"

const Explainer1d03 = () => {
    const [left, setLeft] = useState(0)
    const [right, setRight] = useState(0)
    const [allClicks, setAll] = useState([])
  
    const [total, setTotal] = useState(0)
  
    const handleLeftClick = () => {
    setAll(allClicks.concat('L'))
    // console.log('left before', left)
    // setLeft(left + 1)
    const updatedLeft = left+1;
    setLeft(updatedLeft);
    // console.log('left after', left)
    // setTotal(left + right)
    setTotal(updatedLeft+right)
    }
  
    const handleRightClick = () => {
    setAll(allClicks.concat('R'))
    const updatedRight = right+1;
    setRight(updatedRight);
    // setTotal(left + right)
    setTotal(updatedRight+left);
    }
  return (
    <div>
        {left}
        <button onClick={handleLeftClick}>left</button>
        <button onClick={handleRightClick}>right</button>
        {right}
        <p>{allClicks.join(' ')}</p>
        <p>total {total}</p>
    </div>
  )
}

export default Explainer1d03