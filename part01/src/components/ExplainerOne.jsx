import React from 'react'

const Hello = (props) => {
    console.log(props)
    return(
      <div>
        <p>Hello {props.name}</p>
      </div>
    )
  }

const ExplainerOne = () => {
    const now = new Date();
    const a = 10;
    const b = 20;
    console.log(now, a+b);
    const name = "Sushma";
    const age = 24;
    return(
      <div>
        <p>Greetings! It is {now.toString()}</p>
        <p>{a} + {b} is {a+b}</p>
        {/* <Hello name = "yash"/>
        <Hello name = "saurabh"/>
        <Hello name = "sumit"/> */}
  
        <Hello name = "Yash" age = {24+13}/>
        <Hello name = {name} age = {age}></Hello>
      </div>
    )
}

export default ExplainerOne
