/*Sections covered in this program*/
// Handling Arrays
import React from 'react'
import { useState } from "react"

const Explainer1d02 = () => {
    const [left, setLeft] = useState(0)
    const [right, setRight] = useState(0)
  
    const [allClicks, setAll] = useState([])
  
    const handleLeftClick = () => {
    //   allClicks.push('L'); setAll(allClicks)  //don't do this. this would mutate the array directly
      setAll(allClicks.concat('L')) //concat does not mutate the existing array but rather returns a new copy of the array with the item added to it
      setLeft(left + 1)
    }
  
    const handleRightClick = () => {
      setAll(allClicks.concat('R'))
      setRight(right + 1)
    }
  
    return (
      <div>
        {left}
        <button onClick={handleLeftClick}>left</button>
        <button onClick={handleRightClick}>right</button>
        {right}
  
        <p>{allClicks.join(' ')}</p>
      </div>
    )
}

export default Explainer1d02