/*Sections covered in this program*/
// Rules of Hooks

/*Noteworthy things*/
// The useState function (as well as the useEffect function introduced later on in the course) 
// must not be called from inside of a loop, a conditional expression, or any place that 
// is not a function defining a component.

import React from 'react'
import { useState } from "react"

const Explainer1d05 = () => {
    // these are ok
  const [age, setAge] = useState(0)
  const [name, setName] = useState('Juha Tauriainen')

  if ( age > 10 ) {
    // this does not work!
    const [foobar, setFoobar] = useState(null)
  }

  for ( let i = 0; i < age; i++ ) {
    // also this is not good
    const [rightWay, setRightWay] = useState(false)
  }

  const notGood = () => {
    // and this is also illegal
    const [x, setX] = useState(-1000)
  }

  return (
    <div>
        <p>The useState function (as well as the useEffect function introduced later on in the course) 
        must not be called from inside of a loop, a conditional expression, or any place that 
        is not a function defining a component.</p>
    </div>
  )
}

export default Explainer1d05