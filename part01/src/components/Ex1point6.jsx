/*Exercises covered in this program: 1.6, 1.7, 1.8, 1.9, 1.10, 1.11*/
import React from 'react'
import { useState } from 'react'

const Button = (props) => {
    return ( <button onClick={props.handleClick}>{props.buttontext}</button> );
}

// const StatisticLine = (props) => <div>{props.text} | {props.value}</div>
const StatisticLine = (props) => <tr><td>{props.text}</td><td>{props.value}</td></tr>

const Statistics = (props) => {
    const netvotes = props.curStats.bad+props.curStats.neutral+props.curStats.good;
    if(!(props.curStats.good || props.curStats.neutral || props.curStats.bad))
        return <h4>No feedback given</h4>
    return (
        // <ul>
        //     {/* <li>good {props.curStats.good} </li>
        //     <li>neutral {props.curStats.neutral} </li>
        //     <li>bad {props.curStats.bad} </li>
        //     <li>all {props.curStats.bad+props.curStats.neutral+props.curStats.good} </li>
        //     <li>average {netvotes/3} </li>
        //     <li>positive {(props.curStats.good/netvotes)*100} </li> */}

        //     <li><StatisticLine text = "good" value = {props.curStats.good}/> </li>
        //     <li><StatisticLine text = "neutral" value = {props.curStats.neutral}/> </li>
        //     <li><StatisticLine text = "bad" value = {props.curStats.bad}/> </li>
        //     <li><StatisticLine text = "all" value = {netvotes}/> </li>
        //     <li><StatisticLine text = "average" value = {netvotes/3}/> </li>
        //     <li><StatisticLine text = "positive" value = {(props.curStats.good/netvotes)*100}/> </li>
        // </ul>
        <table>
            <tbody>
            <StatisticLine text = "good" value = {props.curStats.good}/>
            <StatisticLine text = "neutral" value = {props.curStats.neutral}/>
            <StatisticLine text = "bad" value = {props.curStats.bad}/>
            <StatisticLine text = "all" value = {netvotes}/>
            <StatisticLine text = "average" value = {netvotes/3}/>
            <StatisticLine text = "positive" value = {(props.curStats.good/netvotes)*100}/>
            </tbody>
        </table>
    )
}

const Ex1point6 = () => {
    // save clicks of each button to its own state
    const [good, setGood] = useState(0)
    const [neutral, setNeutral] = useState(0)
    const [bad, setBad] = useState(0)
    const thestatsobj = {good:good, bad:bad, neutral:neutral};

    const set2good = () => () => setGood(good+1);
    const set2neutral = () => () => setNeutral(neutral+1);
    const set2bad = () => () => setBad(bad+1);

    return (
        <div>
            <h2>Give Feedback</h2>
            <Button handleClick = {set2good()} buttontext = "Good" />
            <Button handleClick = {set2neutral()} buttontext = "Neutral" />
            <Button handleClick = {set2bad()} buttontext = "Bad" />

            <h2>Statistics</h2>
            <Statistics curStats = {thestatsobj} />
        </div>
    )
}

export default Ex1point6