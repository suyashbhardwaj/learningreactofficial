/*Sections covered in this program*/
/* 
    Event Handling Revisited, 
    A function that returns a function, 
    Passing Event Handlers to Child Components, 
    Do Not Define Components Within Components
*/

/*Noteworthy things*/
/*
    function returning a function as an event handler to set state variables
    we shouldn't define a component within a component
*/

import React from 'react'
import { useState } from "react"

const Display = props => <div>{props.value}</div>
const Button = (props) => (
    <button onClick={props.handleClick}>
        {props.text}
    </button>
    )

const Explainer1d07 = () => {
    const [value, setValue] = useState(10)
    const handleClick = () => console.log('clicked the button');
    // const hello = () => {    const handler = () => console.log('hello world');    return handler  }
    // const hello = (who) => {
    //     const handler = () => { console.log('hello', who) }
    //     return handler
    //     }
    
    // const hello = (who) => {
    //     return () => {
    //       console.log('hello', who)
    //     }
    // }

    // const hello = (who) =>
    //     () => {
    //         console.log('hello', who)
    //     }
    
    /*    ref 2   fn returns fn  that displays a msg */
    const hello = (who) => () => { console.log('hello', who) }  
    /*so hello is now a reference to a parameterized function that itself returns a reference to another function which just displays a message*/

    /* event handlers that set the state of the component to a given value as under*/
    /*just call them rightaway*/
    const setToValue = (newValue) => () => {
        console.log('value now', newValue)  // print the new value to console
        setValue(newValue)
        }
    
    /* PLEASE! NO COMPONENTS HERE INSIDE..*/
    // Do not define components inside another component
    // const Display = props => <div>{props.value}</div>    //No, not here!
    
    return (
        <div>
            {/* {value} */}
            <Display value={value} />
            <br />
            {/* <button>reset to zero</button> */}
            {/* <button onClick="crap...">button</button>   an event handler can't be a string */}
            {/* <button onClick={value + 1}>button</button> can't be a value */}
            {/* <button onClick={value = 0}>button</button> can't be an assignment */}
            {/* <button onClick={console.log('clicked the button')}>button</button> can't be a function call unless it returns a reference to a function, would lead to display of message & then inactivity */}
            {/* <button onClick={setValue(0)}>button</button> this would lead to an infinite recursion due to component re-rendering */}
            <button onClick={handleClick}>Ref2Fnbutton</button> {/*this, reference to a function, is fine. will do...*/}
            <button onClick={hello()}>FnCallbutton</button> {/*this function call returning us a ref to function will do*/}

            <button onClick={hello('world')}>button</button>
            <button onClick={hello('react')}>button</button>
            <button onClick={hello('function')}>button</button>

            {/* buttons to set state values  */}
            <button onClick={setToValue(1000)}>thousand</button>
            <button onClick={setToValue(0)}>reset</button>
            <button onClick={setToValue(value + 1)}>increment</button>

            {/* Passing Event Handlers to Child Components - incorrect in online reference*/}
            {/* <Button handleClick={() => setToValue(1000)} text="Thousand" />
            <Button handleClick={() => setToValue(0)} text="reset" />
            <Button handleClick={() => setToValue(value + 1)} text="increment" /> */}

            <Button handleClick={setToValue(1000)} text="Thousand" />
            <Button handleClick={setToValue(0)} text="reset" />
            <Button handleClick={setToValue(value + 1)} text="increment" />

        </div>
        )
    }

export default Explainer1d07