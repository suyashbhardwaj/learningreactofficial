/*Sections covered in this program*/
// Conditional Rendering, Old React, & Debugging React Applications

/*Noteworthy things*/
// The History component renders completely different React elements depending on the state of the application. 
// This is called conditional rendering.
import React from 'react'
import { useState } from "react"

const History = (props) => {  
    if (props.allClicks.length === 0) {
        // debugger
        return (
            <div>
                the app is used by pressing the buttons
            </div>
            )  
        }  
        
        return ( <div>button press history: {props.allClicks.join(' ')} </div>  )
    }

const Button = ({ handleClick, text }) => (  <button onClick={handleClick}>    {text}  </button>)

const Explainer1d04 = () => {
    const [left, setLeft] = useState(0)
    const [right, setRight] = useState(0)
    const [allClicks, setAll] = useState([])
  
    // const [total, setTotal] = useState(0)
  
    const handleLeftClick = () => {
    setAll(allClicks.concat('L'))
    // console.log('left before', left)
    // setLeft(left + 1)
    const updatedLeft = left+1;
    setLeft(updatedLeft);
    // console.log('left after', left)
    // setTotal(left + right)
    // setTotal(updatedLeft+right)
    }
  
    const handleRightClick = () => {
    setAll(allClicks.concat('R'))
    const updatedRight = right+1;
    setRight(updatedRight);
    // setTotal(left + right)
    // setTotal(updatedRight+left);
    }
    
//   debugger  
  return (
    <div>
        {left}
        {/* <button onClick={handleLeftClick}>left</button>
        <button onClick={handleRightClick}>right</button> */}
        <Button handleClick={handleLeftClick} text='left' />
        <Button handleClick={handleRightClick} text='right' />
        {right}
        {/* <p>{allClicks.join(' ')}</p>
        <p>total {total}</p> */}
        <History allClicks={allClicks} />
    </div>
  )
}

export default Explainer1d04