const Part = (props) => { return (<p>{props.title} {props.marks}</p>) }
const Header = (props) => { return( <h4>{props.course.name}</h4> ); }
const Content = (props) => { 
  return (
    <>
      {/* <Part title = {props.course.parts[0].name} marks = {props.course.parts[0].exercises}/>
      <Part title = {props.course.parts[1].name} marks = {props.course.parts[1].exercises}/>
      <Part title = {props.course.parts[2].name} marks = {props.course.parts[2].exercises}/> */}
      {props.course.parts.map( (part,i) => <Part key = {i} title = {part.name} marks = {part.exercises}/>)}
    </>
    )
  }

const Total = (props) => { 
    let totex = 0;
    props.course.parts.forEach(part => { totex += part.exercises });
    return (<p><b> total of {totex} exercises</b></p>);
    }

const Course = ({ course }) => {
    return (
        <>
            <div>
            <Header course={course} />
            <Content course = {course} />
            <Total course = {course}/>
            </div>
        </>
    )
}

export default Course;