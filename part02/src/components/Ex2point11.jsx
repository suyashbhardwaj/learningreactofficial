import axios from 'axios'
import { useEffect, useState } from 'react'

const Ex2point11 = () => {
  const [persons, setPersons] = useState([])

  useEffect(()=> {
    axios.get('http://localhost:3001/persons').then(response => {
        const persons = response.data;
        console.log(persons);
        setPersons(persons);
  })},[]);
  
  return (
    <div>
        {persons.map(person => {})}
    </div>
  )
}

export default Ex2point11
