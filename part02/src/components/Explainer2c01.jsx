import React from 'react'
import axios from 'axios'
import { useState, useEffect } from 'react'

const Explainer2c01 = () => {
    const [notes, setNotes] = useState([])

    axios.get('http://localhost:3001/notes').then(response => {
    const notes = response.data;
    console.log(notes);
    })
    
    // By default, effects run after every completed render, but you can choose to fire it only when certain values have changed.
    /*useEffect(() => {
        console.log('effect')
        axios
            .get('http://localhost:3001/notes')
            .then(response => {
                console.log('promise fulfilled'); 
                setNotes(response.data)})}, []
        )*/  

    useEffect(() => {
        console.log('effect')
        const eventHandler = response => {
            console.log('promise fulfilled')
            setNotes(response.data)
            }
          
        const promise = axios.get('http://localhost:3001/notes')
            promise.then(eventHandler)
        }, [])
        
    console.log('render', notes.length, 'notes')
    return (
        <div>
        
        </div>
  )
}

export default Explainer2c01
