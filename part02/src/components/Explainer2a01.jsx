import React from 'react'

const Note = ({ note }) => {  return (    <li>{note.content}</li>  )}

// const Explainer2a01 = (props) => {
    const Explainer2a01 = ({notes}) => {    
    // const { notes } = props;

    return (
        <div>
          <h1>Notes</h1>
          <ul>
            {/* <li>{notes[0].content}</li>
            <li>{notes[1].content}</li>
            <li>{notes[2].content}</li> */}
            {/* {notes.map(note => <li key = {note.id}>{note.content}</li>)} // One way to resolve: Each child in a list should have a unique "key" prop */}
            {/* {notes.map((note,i) => <li key = {i} >{note.content}</li>)} //Another way to resolve, the index of element is set up as key */}
            {notes.map(note => <Note key={note.id} note={note}/>)}

          </ul>
        </div>
      )
}

export default Explainer2a01